/*
 * Binary Search Tree Interface
 * Copyright (C) 2022  R. J. Swedlow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _USERIO_H_
#define _USERIO_H_

#include <stddef.h>
#include "bst.h"

/*
 * user_input():  Prompt user at run-time for ASCII input.
 *
 * Preconditions:
 *     1.  Question or other `prompt` message to the user is passed by pointer
 *         to this function.  Or, NULL is passed for no console text message.
 *     2.  User input at run-time does not exceed 512-character buffer space.
 * Postconditions:
 *     1.  A working pointer to the C-standards-conforming string storing
 *         all of the user's input from this line is returned.
 *     2.  Data type manipulation or conversion will have to be done separate.
 */
extern char* user_input(const char* prompt);

/*
 * draw_left_edge():  Draw a line with positive slope, chaining from a parent
 *                    node to its left child.
 *
 * Preconditions:
 *     1.  x and y offset in 2-D ASCII array `graph` are passed by pointer.
 * Postconditions:
 *     1.  x and y are recursively updated one-by-one as each "pixel" in the
 *         2-D map is written, for duration `dist` iterations/recursions.
 *     2.  The final coordinate (x, y) "returned" by this function should
 *         be a point below and to the left of where the drawing finished.
 */
extern int draw_left_edge(
    char** graph, unsigned int dist,
    size_t* x, size_t* y
);

/*
 * draw_right_edge():  Draw a line with negative slope, chaining from a parent
 *                     node to its right child.
 *
 * Preconditions:
 *     1.  x and y offset in 2-D ASCII array `graph` are passed by pointer.
 * Postconditions:
 *     1.  x and y are recursively updated one-by-one as each "pixel" in the
 *         2-D map is written, for duration `dist` iterations/recursions.
 *     2.  The final coordinate (x, y) "returned" by this function should
 *         be a point below and to the right of where the drawing finished.
 */
extern int draw_right_edge(
    char** graph, unsigned int dist,
    size_t* x, size_t* y
);

/*
 * btsprintf():  Behave like the C standard's sprintf() function except, in
 *               this case, print all numbers and edges of the tree view of
 *               a binary tree.
 *
 * Preconditions:
 *     1.  `graph` has already been allocated to store ASCII "scanlines" in
 *         the spirit of a two-dimensional array of characters.
 *     2.  The `height` passed by value was correctly determined as a
 *         function of the binary tree's depth.
 * Postconditions:
 *     1.  Each node's key and the connecting edges are graphed into an ASCII
 *         map for a pretty-enough tree view.
 */
extern int btsprintf(
    char** graph, const bsnode* tree,
    size_t x, size_t y, size_t height
);

/*
 * bststr():  Render an ASCII-graphical representation of a given binary tree.
 *
 * Preconditions:
 *     1.  There must be enough available memory to allocate the text buffers.
 * Postconditions:
 *     1.  btsprintf() is recursively called for each data aspect of the tree
 *         view until a finalized ASCII representation has been computed in
 *         memory; then, each line of text is printed to standard output.
 */
extern void bststr(const bsnode* tree);

#endif
