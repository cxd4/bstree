/*
 * Binary Search Tree Interface
 * Copyright (C) 2022  R. J. Swedlow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _BST_H_
#define _BST_H_

#include <stddef.h>

typedef struct btree_node* bt_ptr;
typedef struct btree_node {
    short int key;
    bt_ptr left_child; /* if bsnode, lesser */
    bt_ptr right_child; /* if bsnode, greater */
} bnode, bsnode;
/* We can use either type name.  BT = any binary tree; BST = sorted L < N < R */

/*
 * btheight():  internal routine to get the height of any general binary tree
 *
 * Preconditions:
 *     No special requirements of note.
 * Postconditions:
 *     1.  The depth of the tree (a.k.a., the "height") is returned.
 *     2.  If the root node has no children, there are no edges and so 0 will
 *         be the depth.
 *     3.  If there is no root node, we treat the depth to be negative one.
 */
extern int btheight(const bsnode* tree);

/*
 * btalloc():  internal routine for allocating new memory with enough space to
 *             store a new node that can fit in any generic binary tree
 *
 * Preconditions:
 *     1.  You must have enough available RAM for memory to allocate.
 *     2.  The `key` should not be so large a number that it will interfere
 *         with the ability to print the entire binary tree view on-screen.
 *     3.  The `key` should not be a negative number.
 * Postconditions:
 *     1.  A pointer to the newly allocated binary node is returned, with
 *         both children node pointers insured to be pre-set to NULL.
 */
extern bnode* btalloc(short key);

/*
 * bstattach():  internal routine for attaching newly allocated node to BST
 *
 * Preconditions:
 *     1.  `new_node` is a pointer to a newly created node whose key obeys
 *         BST ordering.
 *     2.  `tree` is not NULL.  Do not attempt to attach to an empty tree.
 *         In this case we could simply call bst_new().
 *     3.  By virtue of precondition #1, a pre-existing node which already has
 *         this key will disqualify an attempt to attach the new node.
 * Postconditions:
 *     1.  `new_node` will belong to what was formerly a leaf node in the
 *         writable BST `tree`.  The assigned parent leaf node will naturally
 *         be the only solution that maintains the properties of a BST.
 *     2.  If precondition #3 fails, the return value will be a negative
 *         integer.  Otherwise, the return value is zero upon completion.
 */
extern int bstattach(bsnode* tree, const bsnode* new_node);

/*
 * bst_new():  Install a new binary sub-tree.
 *
 * Preconditions:
 *     1.  You must have enough available RAM for memory to allocate.
 *     2.  Key regulations will be processed at run-time by btalloc()
 *         after fetching user input.
 * Postconditions:
 *     1.  bstattach() will link the appropriate leaf node as the parent of
 *         the newly allocated node, based on its key.
 *     2.  If the requested `tree` of which the new node is to be attached
 *         is a NULL pointer, a single-node BST with depth 0 is returned.
 *     3.  Otherwise, the return value should be unchanged from `tree`.
 */
extern bsnode* bst_new(bsnode* root);

/*
 * bst_delete():  Delete from the BST a node by specified key.
 *
 * Preconditions:
 *     1.  The pointer to binary tree passed to this function is a pointer to
 *         a more specifically formatted tree:  a BST.
 * Postconditions:
 *     1.  A pointer to the resulting binary tree or subtree after the
 *         deletion has been performed is returned.
 *     2.  If no matching node could be deleted, an error is printed.
 *         The return value in this case should be unchanged from `root`.
 *     3.  We ensure that the corresponding amount of memory is freed up.
 */
extern bsnode* bst_delete(bsnode* tree, int key);

/*
 * bst_search():  Find an existing node by specified key within a BST.
 *
 * Preconditions:
 *     1.  We should be enforcing this on our own within this program, but,
 *         the tree should not have two or more copies of the same node key.
 * Postconditions:
 *     1.  A working pointer to the node located to have the requested key
 *         is returned.
 *     2.  If no node meeting the specified criteria could be found in the
 *         specified tree or subtree, NULL is returned.
 */
extern bsnode* bst_search(const bsnode* tree, int key);

/*
 * bst_inorder_traversal():  Print each node's key in ascending order.
 *
 * Preconditions:
 *     1.  The tree passed by pointer meets the rules of a BST.
 * Postconditions:
 *     1.  We conduct a traversal in the L-N-R order to print all keys in the
 *         BST with a separating space, effectively in ascending order.
 *     2.  The number of nodes traversed upon is returned.
 */
extern size_t bst_inorder_traversal(const bsnode* tree);

/*
 * bst_rnl_traversal():  Print each node's key in descending order.
 *
 * Preconditions:
 *     1.  The tree passed by pointer meets the rules of a BST.
 * Postconditions:
 *     1.  We conduct a traversal in the R-N-L order to print all keys in the
 *         BST with a separating space, effectively in descending order.
 *     2.  The number of nodes traversed upon is returned.
 */
extern size_t bst_rnl_traversal(const bsnode* tree);

/*
 * clipmax(m, n):  Select the greater of two quantities.
 *
 * Preconditions:
 *     no special prerequisites of note
 * Postconditions:
 *     1.  The greater between the both parameters passed by value to this
 *         function is returned.
 *     2.  If m == n, that value is returned.
 */
extern int clipmax(int m, int n);

/*
 * bst_cost_traversal():  Calculate the sum of keys while traversing a path.
 *
 * Preconditions:
 *     1.  The node bound by specified `key` to the call exists in the tree.
 *     2.  The specified `tree` references a conformant BST.
 * Postconditions:
 *     1.  A nonnegative total accumulating all keys in nodes traversed is
 *         returned by a call to this function, if the node does exist.
 *     2.  Should the node not exist, a large negative integer is returned.
 */
extern long int bst_cost_traversal(const bsnode* tree, int key);

#endif
