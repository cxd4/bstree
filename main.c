/*
 * Binary Search Tree Interface
 * Copyright (C) 2022  R. J. Swedlow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "bst.h"
#include "userio.h"

int
main(void)
{
    long int cost;
    bsnode* root = NULL;
    int character = '*'; /* default storage; means last entry was implemented */
    const char MENU[] =
        "Please choose from the following menu:\n"\
        "    S:  Search BST for node by a given key\n"\
        "    I:  Insert a new node from user-defined key\n"\
        "    X:  Delete an existing node from BST\n"\
        "    V:  Display tree-view of all BST nodes on the console\n"\
        "    L:  Print a list of all keys sorted in descending order\n"\
        "    C:  Calculate the \"cost\" of a traversible BST path\n"\
        "Anything else to exit the program.\n"\
    ;

    do {
        puts(MENU);
        character = getchar();
        while (getchar() != '\n')
            ;

        switch (toupper(character)) {
        case '*':
            exit(EXIT_SUCCESS);
        case 'S': /* Search BST for node. */
            puts(
                bst_search(root, 0) != NULL ?
                "Node with specified key was found." :
                "No node with specified key yet exists."
            );
            character = '*';
            break;
        case 'I': /* Insert new node into the BST. */
            root = bst_new(root);
            character = '*';
            break;
        case 'X': /* Delete existing node from BST. */
            root = bst_delete(root, 0);
            character = '*';
            break;
        case 'V': /* View a printed display of all nodes in the tree. */
            bststr(root);
            character = '*';
            break;
        case 'L': /* Print a sorted list of all BST keys. */
            printf("\nTotal of %lu nodes.\n", bst_rnl_traversal(root));
            character = '*';
            break;
        case 'C': /* Show the sum of all keys from the root to a given node. */
            cost = bst_cost_traversal(root, 0);
            character = '*';
            if (cost < 0) {
                fputs("Error:  No node with that key exists.\n", stderr);
                break;
            }
            printf("Cost of traversal:  %li\n", cost);
            break;
        }
    } while (character == '*');
    printf("Exiting after character entry %d ('%c').\n", character, character);
    return 0;
}
