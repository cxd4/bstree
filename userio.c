/*
 * Binary Search Tree Interface
 * Copyright (C) 2022  R. J. Swedlow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bst.h"
#include "userio.h"

char*
user_input(const char* prompt)
{
    static char linebuf[512];
    char* result;
    int success;

    success = fputs(prompt, stdout);
    if (success < 0)
        abort();
    result = fgets(&linebuf[0], sizeof(linebuf), stdin);
    assert(result != NULL);
    return (result);
}

int
draw_left_edge(char** graph, unsigned int dist, size_t* x, size_t* y)
{
    if (dist == 0)
        return 0;
    graph[*y][*x] = '/';
    *x -= 1;
    *y += 1;
    return (1 + draw_left_edge(graph, dist - 1, x, y));
}
int
draw_right_edge(char** graph, unsigned int dist, size_t* x, size_t* y)
{
    if (dist == 0)
        return 0;
    graph[*y][*x] = '\\';
    *x += 1;
    *y += 1;
    return (1 + draw_right_edge(graph, dist - 1, x, y));
}

int
btsprintf(char** graph, const bsnode* tree, size_t x, size_t y, size_t height)
{
    int chars_written;
    size_t x2, y2;

    if (tree == NULL)
        return 0;
#if 1
    graph[y][x - 0] = '0' + (char)(tree->key % 10);
    graph[y][x - 1] = '0' + (char)(tree->key / 10);
    chars_written = 2;
#else
    chars_written = sprintf(&graph[y][x - 1], "%02i", tree->key);
    assert(chars_written == 2);
#endif

    if (tree->left_child != NULL) {
        y2 = y + 1;
        x2 = x - 1;
        chars_written += draw_left_edge(graph, height/2 - 1, &x2, &y2);
        chars_written +=
            btsprintf(graph, tree->left_child, x2 - 0, y2, height / 2);
    }
    if (tree->right_child != NULL) {
        y2 = y + 1;
        x2 = x - 0;
        chars_written += draw_right_edge(graph, height/2 - 1, &x2, &y2);
        chars_written +=
            btsprintf(graph, tree->right_child, x2 + 1, y2, height / 2);
    }
    return (chars_written);
}

void
bststr(const bsnode* tree)
{
    register size_t x, y;
    int tree_height;
    char** ascii_map;
    size_t map_height, map_width;

    tree_height = btheight(tree);
    printf("Graphing ASCII map of binary tree with height %i:\n", tree_height);
    if (tree_height < 0) {
        return;
    }

    map_height = 1U << (tree_height + 1);
    map_width = 2 * map_height;
    ascii_map = (char **)malloc(map_height * sizeof(void *));
    assert(ascii_map != NULL);
    for (y = 0; y < map_height; y++) {
        ascii_map[y] = (char *)malloc(map_width * sizeof(char));
        assert(ascii_map[y] != NULL);
        memset(ascii_map[y], ' ', map_width);
        ascii_map[y][map_width - 1] = '\0';
        ascii_map[y][map_width - 2] = '\n';
    }

    btsprintf(ascii_map, tree, map_width / 2 - 1, 0, map_height);
    for (y = 0; y < map_height; y++) {
        for (x = 0; x < map_width; x++) {
            putchar(ascii_map[y][x]);
        }
        free(ascii_map[y]);
    }
    free(ascii_map);
}
