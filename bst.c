/*
 * Binary Search Tree Interface
 * Copyright (C) 2022  R. J. Swedlow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include "userio.h"
#include "bst.h"

/*
 * clipmax(m, n):  Select the greater of two quantities.
 * Preconditions:
 *     no special prerequisites of note
 * Postconditions:
 *     1.  The greater between the both parameters passed by value to this
 *         function is returned.
 *     2.  If m == n, that value is returned.
 */
int
clipmax(int m, int n)
{
    int selection_pool[2];

    selection_pool[0] = m;
    selection_pool[1] = n;
    return (selection_pool[(m - n < 0) ? 1 : 0]);
}

int
btheight(const bnode* tree)
{
    if (tree == NULL)
        return -1;
    return (
        1 +
        clipmax(
            btheight(tree->left_child),
            btheight(tree->right_child)
        )
    );
}

bnode*
btalloc(short key)
{
    bnode* new_node;
    int error_count = 0;

    new_node = (bnode *)malloc(sizeof(bnode));
    while (new_node == NULL && error_count < 10) {
        ++error_count;
        fputs("ERROR allocating memory for new node.  Retrying...\n", stderr);
        new_node = (bnode *)(
            malloc(sizeof(bnode))
        );
    }

    new_node->key = key;
    new_node->left_child = NULL;
    new_node->right_child = NULL;
    return (new_node);
}

int
bstattach(bsnode* tree, const bsnode* new_node)
{
    bsnode** next_child_ptr;

    assert(new_node != NULL);
    assert(tree != NULL);
    if (new_node->key == tree->key)
        return -1;

    next_child_ptr = (
        new_node->key < tree->key ?
        &tree->left_child : &tree->right_child
    );
    if (*next_child_ptr == NULL) {
        *next_child_ptr = (bsnode *)new_node;
        return 0;
    }
    return bstattach(*next_child_ptr, new_node);
}

bsnode*
bst_new(bsnode* root)
{
    long int integer;
    short int new_key;
    bsnode* new_node;

    do {
        integer = strtol(
            user_input("New node from what key (1 to 99, please)?  "),
            NULL, 10
        );
    } while (integer < 1 || integer > 99);
    new_key = (short)integer;

    new_node = btalloc(new_key);
    if (root == NULL)
        return (new_node);

    if (bstattach(root, new_node) < 0) {
        fprintf(stderr, "Cannot insert already existing key %i.\n", new_key);
        free(new_node);
    }
    return (root);
}

bsnode*
bst_search(const bsnode* tree, int key)
{
/* Pass a 0 key to this function to loop prompting user for key at run-time. */
    long int key_request = key;
    while (key_request < 1 || key_request > SHRT_MAX) {
        key_request = strtol(user_input("Search for what key?  "), NULL, 0);
        key = (short)key_request;
    }

    if (tree == NULL || tree->key == key)
        return ((bsnode *)tree);
    return (
        key < tree->key ?
        bst_search(tree->left_child, key) :
        bst_search(tree->right_child, key)
    );
}

/* useful for finding in-order successor, if ran on right-subtree in a BST */
static bsnode*
bstminnode(const bsnode* tree)
{
    if (tree == NULL || tree->left_child == NULL)
        return ((bsnode *)tree);
    return bstminnode(tree->left_child);
}
/* useful for finding in-order predecessor, if ran on left-subtree in a BST */
static bsnode*
bstmaxnode(const bsnode* tree)
{
    if (tree == NULL || tree->right_child == NULL)
        return ((bsnode *)tree);
    return bstmaxnode(tree->right_child);
}

bsnode*
bst_delete(bsnode* tree, int key)
{
/* Pass a 0 key to this function to loop prompting user for key at run-time. */
    long int key_request = key;
    while (key_request < 1 || key_request > INT_MAX) {
        key_request = strtol(user_input("Delete node by key:  "), NULL, 0);
        key = (int)key_request;
    }

/*
 * Base case number 1:
 * We've searched all the way through to the end of the sorted tree but could
 * never find the node requested for deletion.  Cancel and delete nothing.
 */
    if (tree == NULL) {
        fputs(
            "No node fitting the requested criteria to be deleted.\n",
            stderr
        );
        return NULL;
    }

/*
 * Base case number 2:
 * We've found the node, but may still need to conduct further recursion to
 * handle the cleanup involved in the case of nodes with two children.
 */
    if (tree->key == key) {
        bsnode* exchange_node;
        const unsigned int children =
            (tree->left_child  != NULL ? 1 : 0)
          + (tree->right_child != NULL ? 1 : 0)
        ;

        switch (children) {
        case 0:
            free(tree); /* In this case, the "tree" is just a lone leaf node. */
            return NULL; /* The parent node's pointer gets our return value. */
        case 1:
            exchange_node = (
                tree->left_child == NULL ?
                tree->right_child :
                tree->left_child
            );
            free(tree);
            return (exchange_node);
        case 2:
            if (btheight(tree->left_child) > btheight(tree->right_child)) {
                exchange_node = bstmaxnode(tree->left_child);
                assert(exchange_node != NULL);
                tree->key = exchange_node->key;
                tree->left_child =
                    bst_delete(tree->left_child, exchange_node->key);
            } else { /* If LST isn't taller than RST, use in-order successor. */
                exchange_node = bstminnode(tree->right_child);
                assert(exchange_node != NULL);
                tree->key = exchange_node->key;
                tree->right_child =
                    bst_delete(tree->right_child, exchange_node->key);
            }
            return (tree);
        default:
            abort(); /* Non-binary trees (3+ children) not implemented here. */
        }
    }

    if (key < tree->key) {
        tree->left_child = bst_delete(tree->left_child, key);
    } else {
        tree->right_child = bst_delete(tree->right_child, key);
    }
    return (tree);
}

size_t
bst_inorder_traversal(const bsnode* tree)
{
    size_t node_count;

    if (tree == NULL)
        return (node_count = 0);
    node_count = 1;

    node_count += bst_inorder_traversal(tree->left_child);
    printf("%02i ", tree->key);
    node_count += bst_inorder_traversal(tree->right_child);
    return (node_count);
}

size_t
bst_rnl_traversal(const bsnode* tree)
{
    size_t node_count;

    if (tree == NULL)
        return (node_count = 0);
    node_count = 1;

    node_count += bst_rnl_traversal(tree->right_child);
    printf("%02i ", tree->key);
    node_count += bst_rnl_traversal(tree->left_child);
    return (node_count);
}

long int
bst_cost_traversal(const bsnode* tree, int key)
{
    const bsnode* next_subtree;
/* Pass a 0 key to this function to loop prompting user for key at run-time. */
    long int key_request = key;
    while (key_request < 1 || key_request > INT_MAX) {
        key_request = strtol(user_input("Cost path to what key?  "), NULL, 0);
        key = (int)key_request;
    }

    if (tree == NULL)
        return LONG_MIN;
    if (tree->key == key)
        return (key);

    next_subtree = (key < tree->key ? tree->left_child : tree->right_child);
    return (tree->key + bst_cost_traversal(next_subtree, key));
}

